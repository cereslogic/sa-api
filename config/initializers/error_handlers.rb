# By default, an unexpected exception will cause the ExceptionHandler to render
# '{"errors":["Server error"]}' with a 500 status. You can add separate handlers
# to return a custom body and status with a hash:
#
# ExceptionHandler.register(
#     "DataObjects::IntegrityError" => {body: {errors: ["Username taken"]}.to_json,
#                                       status: 422})
#
# or by specifying a Handler class to use:
#
# ExceptionHandler.register(
#     "DataObjects::IntegrityError" => DataObjectsIntegrityErrorHandler)
#
# The class just has to respodn to body and status:
#
# class DataObjectsIntegrityErrorHandler
#   def body(exception)
#     logger.error "custom logging with #{exception}, possibly?"
#     {errors: ["Username taken"]}.to_json
#   end
#
#   def status
#     422
#   end
# end
#
# which allows the error to be recorded differently or add custom logging if
# for some reason that seems like a good idea in the future.

ExceptionHandler.register(
  "Sequel::UniqueConstraintViolation" => {
    body: {errors: ["Username taken"]}.to_json,
    status: 422
  }
)

ExceptionHandler.register(
  "CloudshineData::Unauthorized" => {status: 401, body: "Unauthorized"}
)

ExceptionHandler.register(
  "ActiveRecord::RecordNotFound" => {
    body: {errors: ["Resource not found."]}.to_json,
    status: 404
  }
)

ExceptionHandler.register("Sequel::ValidationFailed" => HandlerWithMessage)

ExceptionHandler.register("Cloudshine::MissingParameter" => HandlerWithMessage)
