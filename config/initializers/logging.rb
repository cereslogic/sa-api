$logger = Logging.logger['logger']

Logging.color_scheme( 'bright',
  :levels => {
    :info  => :green,
    :warn  => :yellow,
    :error => :red,
    :fatal => [:white, :on_red]
  },
  :date => :cyan,
  :message => :white
)

Logging.appenders.stdout(
  'stdout',
  :layout => Logging.layouts.pattern(
    :pattern => '%p [%d] %-5l %m\n',
    :color_scheme => 'bright'
  )
)

appenders = []
case RACK_ENV
when 'development'
  appenders << Logging.appenders.stdout
  $logger.level = :debug
when 'test'
  appenders << Logging.appenders.stdout
  $logger.level = :off
else
  appenders << Logging.appenders.stdout
  appenders << Logging.appenders.file('logging.log')
  $logger.level = :error
end

$logger.add_appenders(appenders)

# define a logger helper here so non-sinatra classes can use it
def logger
  $logger
end
