APP_ROOT = File.expand_path('../..', __FILE__) unless defined? APP_ROOT
$LOAD_PATH.unshift(File.join(APP_ROOT, 'lib'))

RACK_ENV = ENV['RACK_ENV'] || 'development'

# TODO: Need to make this per-user.
ORIGIN_WHITELIST = ['http://localhost','http://staging.sureaddress.io','http://www.sureaddress.io','http://address.io']

require "rubygems"
require "bundler"
Bundler.require(:default, RACK_ENV.to_sym)
require 'logging'
require 'sinatra'
require 'sinatra/reloader'
require 'securerandom'
require 'json'

require_relative '../services/init'
require_relative '../controllers/init'
require_relative '../models/init'
require_relative '../responses'

# pass the temporary mocks file through ERB so we get some
# decent time data
# MOCKS = YAML.load(ERB.new(File.read("config/mocks.yml")).result)

# Run all the initializers (except database.rb, we already ran that one above).
Dir["#{APP_ROOT}/config/initializers/**/*.rb"].each { |j| require j }

