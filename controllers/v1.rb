require File.join("#{APP_ROOT}/config", 'environment.rb')

require_relative "v1/street_address"

class App < Sinatra::Base
  register Sinatra::Namespace

  namespace '/api' do
    PUBLIC_URLS = ["/ping", "/sessions"]

    before do
      pass if whitelisted?(request)
    end

    # if/when v2 is written, remove application/json from here and add it to the v2
    # version so that becomes the default. -mdd 2013.04.09
    # we could possibly do this in middleware -dnr 2013.04.09
    namespace provides: ["application/vnd.sureaddress-v1+json", "application/json"] do
      register V1::StreetAddress

      get "/ping" do
        Time.now.to_json
      end

      after do
        content_type "application/vnd.sureaddress-v1+json"
      end
    end
  end
end
