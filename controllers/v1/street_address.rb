module V1
  module StreetAddress
    def self.registered(app)
      app.post '/address-query' do
        Sentry.gate(params, request.env)
    
        @delivery_line = DeliveryLineParser.new(params[:street], params[:street2] || '')
        @query         = AddressQuery.new(params, @delivery_line)
        @found         = Address.search(@query.to_query)
        @results       = AddressSerializer.new(@delivery_line, @query, @found).to_hash
        @results.to_json
      end
    end
  end
end
