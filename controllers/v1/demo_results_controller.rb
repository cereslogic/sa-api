class DemoResultsController < ApplicationController
  def show
  end

  def create
    @delivery_line = DeliveryLineParser.new(params[:street], params[:street2])
    @query         = AddressQuery.new(params, @delivery_line)
    @found         = Address.find(@query.to_query, @delivery_line)
    @results       = AddressSerializer.new(@delivery_line, @query, @found).to_hash

    Rails.logger.ap @found

    respond_to do |format|
      format.html
      format.json { render :json => @results.to_json }
    end
  end
end
