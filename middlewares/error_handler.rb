class ErrorHandler
  def initialize(app, options = {})
    @app = app
  end

  def call(env)
    dup._call(env)
  end

  def _call(env)
    @app.call env
  rescue Exception => e
    logger.error "--- rescued #{e.inspect} at #{Time.now} -------------------------------"
    e.backtrace.each do |line|
      logger.error line
    end

    error_handler = ExceptionHandler.new(e)
    # I'm wondering if we should turn this off in development?
    # raise e if RACK_ENV == 'development' || RACK_ENV == 'test'
    Rack::Response.new(error_handler.body, error_handler.status, {}).finish
  end

  def add_handlers(handler_map={})
    @exceptions_map.merge(handler_map)
  end

  private
  attr_reader :exceptions_map
end
