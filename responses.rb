class FourOhFourResponse
  def call(env)
    Rack::Response.new("", 404)
  end
end
