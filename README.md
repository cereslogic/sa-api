Yet another half-finished side-project by mdesjardins.

This is the back-end API portion of a USPS address validation service. The front end is a Rails project called sa-web.

This is basically a Sinatra veneer around Elasticsearch.
