module Cloudshine
  module CommonExceptions
    class MissingParameter < RuntimeError
    end

    def required_parameter(params, required)
      required = [required] if !required.is_a? Array
      required.map!(&:to_s)
      if !((required & params.keys) == required)
        missing = required - params.keys
        raise MissingParameter.new("Missing required parameter(s): #{missing.join(', ')}")
      end
    end

    alias_method :required_parameters, :required_parameter

    def raise_404
      
    end
  end
end

