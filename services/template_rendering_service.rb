module TemplateRenderingService
  class Renderer
    TEMPLATE_DIRECTORY = File.join(File.dirname(__FILE__),'../templates')

    def self.render(template_name, data, options={})
      template = ERB.new File.new(File.join(TEMPLATE_DIRECTORY,template_name)).read, nil, '%'
      template.result(binding)
    end
  end
end
