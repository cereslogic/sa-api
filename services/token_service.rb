class TokenService
  def self.assign!(uid)
    identity = CloudshineData::Identity.for_identity_id(uid)
    identity.token = self.generate_token
    identity.token_issued_at = Time.now
    identity.save
    identity.token
  end

  def self.generate_token
    SecureRandom.uuid
  end

  def self.valid?(token)
    !!CloudshineData::Identity.for_token(token)
  end
end
