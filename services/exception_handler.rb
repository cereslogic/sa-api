class ExceptionHandler
  HANDLERS = {}
  attr_accessor :exception

  def initialize(exception)
    @exception = exception

    @handler_map = HANDLERS
    @handler_map.default = {body: {errors: ["Server error"]}.to_json,
                            status: 500}
  end

  def handler
    return @handler if @handler
    logger.info "using #{handler_map[exception.class.to_s]} to handle a #{exception.class.to_s} exception"
    handler = handler_map[exception.class.to_s]
    @handler = handler.is_a?(Class) ? handler.new(exception) : OpenStruct.new(handler)
  end

  def status
    handler.status
  end

  def body
    handler.body
  end

  def self.register(handler={})
    HANDLERS.merge!(handler)
  end

  private
  attr_accessor :handler_map

end

class Handler
  def initialize(exception)
    @exception = exception
  end

  private
  attr_accessor :exception
end

class HandlerWithMessage < Handler
  def body
    {errors: [exception.message]}.to_json
  end

  def status
    422
  end
end
