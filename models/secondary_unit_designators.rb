#
# See http://pe.usps.gov/text/pub28/28apc_003.htm
#
module SecondaryUnitDesignators
  SECONDARY_UNIT_DESIGNATOR_MAP = {
    'APARTMENT' => 'APT',
    'APT' => 'APT',
    'BASEMENT' => 'BSMT',
    'BSMT' => 'BSMT',
    'BUILDING' => 'BLDG',
    'BLDG' => 'BLDG',
    'DEPARTMENT' => 'DEPT',
    'DEPT' => 'DEPT',
    'FLOOR' => 'FL',
    'FLR' => 'FL',
    'FL' => 'FL',
    'FRONT' => 'FRNT',
    'FRNT' => 'FRNT',
    'HANGER' => 'HNGR',
    'HNGR' => 'HNGR',
    'KEY' => 'KEY',
    'LOBBY' => 'LBBY',
    'LBBY' => 'LBBY',
    'LOT' => 'LOT',
    'LOWER' => 'LOWR',
    'LOWR' => 'LOWR',
    'OFFICE' => 'OFC',
    'OFC' => 'OFC',
    'PENTHOUSE' => 'PH',
    'PH' => 'PH',
    'PIER' => 'PIER',
    'REAR' => 'REAR',
    'ROOM' => 'RM',
    'RM' => 'RM',
    'SIDE' => 'SIDE',
    'SLIP' => 'SLIP',
    'SPACE' => 'SPC',
    'SPC' => 'SPC',
    'STOP' => 'STOP',
    'SUITE' => 'STE',
    'STE' => 'STE',
    'TRAILER' => 'TRLR',
    'TRLR' => 'TRLR',
    'UNIT' => 'UNIT',
    'UPPER' => 'UPPR',
    'UPPR' => 'UPPR'
  }

  SECONDARY_UNIT_DESIGNATORS_WITH_ADDRESSES = [
    'APT',  'BLDG', 'DEPT', 'FL', 'HNGR', 'KEY',  'LOT', 'OFC', 'PH',
    'PIER', 'RM', 'SLIP', 'SPC', 'STOP', 'STE', 'TRLR', 'UNIT' => 'UNIT'
  ]
end
