require 'elasticsearch'

class AddressQuery
  class MissingRequiredParameter < StandardError; end
  class InvalidParameter < StandardError; end

  attr_accessor :input_id, :street, :street2, :city, :state, :zip_code, :addressee, :urbanization, :candidates, :address_number

  def initialize(params, delivery_line)
    @input_id     = params[:input_id]
    @city         = params[:city]
    @state        = params[:state]
    @zip_code     = params[:zipcode]
    @lastline     = params[:lastline]
    @addressee    = params[:addressee]
    @urbanization = params[:urbanization]
    @candidates   = params[:candidates] || 10
    @street2      = params[:street2] || ''
    # scrub_and_validate
    @delivery_line = delivery_line
  end


  def to_query
    @q = {
      query: {
        bool: {
          must: [],
          should: [{
            fuzzy_like_this: {
              fields: ['street_name'],
              like_text: @delivery_line.street_name,
            }
          }]
        }
      }
    }

    add_primary_street_number
    add_suffix
    add_secondary_address_identifier
    add_secondary_address
    add_predirectional
    add_city
    add_state
    add_zip_code

    logger.info ap(@q)
    @q
  end


  def addressee
    return nil if @addressee.blank?
    @addressee.upcase.strip
  end


  protected


  def add_primary_street_number
    street_number = @delivery_line.primary_address_number
    if street_number.present?
      if street_number.strip.match(/^\d+$/)
        # The address number is numeric. Query on the integer fields.
        @q[:query][:bool][:must] << {
          range: {
            addr_primary_low_number: {
              lte: street_number.to_i
            }
          }
        } 

        @q[:query][:bool][:must] << {
          range: {
            addr_primary_high_number: {
              gte: street_number.to_i
            }
          }
        }
      else
        # The address has other junk in it. Query on the string fields.
        # The address has other junk in it. Query on the string fields.
        @q[:query][:bool][:should] << {
          query_string: {
            default_field: 'addr_primary_low_number_string', 
            query: street_number
          }
        }
        @q[:query][:bool][:should] << {
          query_string: {
            default_field: 'addr_primary_high_number_string', 
            query: street_number
          }
        }
      end

      odd_even_code = (street_number.to_i % 2 == 0 ? 'E' : 'O')
      odd_even_codes = ['B', odd_even_code].join(' OR ')
      @q[:query][:bool][:should] << {
        query_string: {
          default_field: 'addr_primary_odd_even_code',
          query: odd_even_codes
        }
      }
    end
  end

  def add_suffix
    suffix = @delivery_line.suffix
    if suffix.present?
      @q[:query][:bool][:should] << {
        query_string: {
          default_field: 'street_suffix_abbrev',
          query: suffix
        }
      }
    end
  end
  
  def add_secondary_address_identifier
    secondary_address_identifier = @delivery_line.secondary_address_identifier
    if secondary_address_identifier.present?
      @q[:query][:bool][:should] << {
        query_string: {
          default_field: 'addr_secondary_abbrev',
          query: secondary_address_identifier
        }
      }
    end
  end

  def add_secondary_address
    secondary_address = @delivery_line.secondary_address
    if secondary_address.present?
      if secondary_address.strip.match(/^\d+$/)
        # The address is numeric. Query on the integer fields.
        @q[:query][:bool][:should] << {
          range: {
            addr_secondary_low_number: {
              lte: secondary_address.to_i
            }
          }
        } 

        @q[:query][:bool][:should] << {
          range: {
            addr_secondary_high_number: {
              gte: secondary_address.to_i
            }
          }
        }
      else
        # The address has other junk in it. Query on the string fields.
        @q[:query][:bool][:should] << {
          range: {
            addr_secondary_low_number_string: {
              lte: secondary_address.try(:downcase)
            }
          }
        }
        @q[:query][:bool][:should] << {
          range: {
            addr_secondary_high_number_string: {
              gte: secondary_address.try(:downcase)
            }
          }
        }
      end

      odd_even_code = (secondary_address % 2 == 0 ? 'E' : 'O')
      odd_even_codes = ['B', odd_even_code]
      @q[:query][:bool][:should] << {
        terms: {
          addr_secondary_odd_even_code: odd_even_codes
        }
      }
    end
  end

  def add_predirectional
    predirectional = @delivery_line.predirectional
    if predirectional.present?
      @q[:query][:bool][:must] << {
        query_string: {
          default_field: 'street_pre_direction_abbrev',
          query: predirectional
        }
      }
    end
  end

  def add_city
    @q[:query][:bool][:must] << {
      fuzzy_like_this: {
        fields: ['city_name'],
        like_text: @city
      }
    } if @city.present?
  end

  def add_state
    if @state.present?
      @state.capitalize!
      if State::LIST.include?(@state)
        @state = State::ABBREVIATIONS[@state]
      end
      
      @q[:query][:bool][:must] << {
        query_string: {
          default_field: 'state_abbrev',
          query: @state.upcase
        }
      }
    end
  end

  def add_zip_code
    @q[:query][:bool][:should] << {
      query_string: {
        default_field: 'zip_code',
        query: @zip_code
      }
    } if @zip_code.present?
  end
end
