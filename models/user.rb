class User
  attr_accessor :id
  
  include Redis::Objects
  
  list :auth_tokens
  hash_key :auth_token_descriptions
  value :recharge_threshold

  counter :credits_remaining
  counter :usage_today
end
