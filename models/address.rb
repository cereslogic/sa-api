require 'elasticsearch/persistence/model'

class Address
  include Elasticsearch::Persistence::Model

  attribute :copyright_detail_code, String
  attribute :zip_code, String
  attribute :update_key_no, String
  attribute :action_code, String
  attribute :record_type, String
  attribute :carrier_route_id, String 
  attribute :street_pre_direction_abbrev, String 
  attribute :street_name, String 
  attribute :suggest_street_name, String, default: {}, mapping: { type: 'completion', payloads: true }
  attribute :street_suffix_abbrev, String 
  attribute :street_post_direction_abbrev, String 
  attribute :addr_primary_low_number, Integer
  attribute :addr_primary_high_number, Integer
  attribute :addr_primary_low_number_string, String
  attribute :addr_primary_high_number_string, String
  attribute :addr_primary_odd_even_code, String 
  attribute :building_or_firm_name, String 
  attribute :addr_secondary_abbrev, String
  attribute :addr_secondary_low_number, Integer
  attribute :addr_secondary_high_number, Integer
  attribute :addr_secondary_low_number_string, String
  attribute :addr_secondary_high_number_string, String
  attribute :addr_secondary_odd_even_code, String
  attribute :zip_add_on_low_number_sector_number, String
  attribute :zip_add_on_low_number_segment_number, String
  attribute :zip_add_on_high_number_sector_number, String
  attribute :zip_add_on_high_number_segment_number, String
  attribute :base_alt_code, String
  attribute :lacs_status_ind, String
  attribute :govt_building_indicator, String
  attribute :finance_number, String
  attribute :state_abbrev, String
  attribute :county_number, Integer
  attribute :city_name, String
  attribute :congressional_district_number, String
  attribute :municipality_city_state_key, String
  attribute :urbanization_city_state_key, String
  attribute :preferred_last_line_city_state_key, String
  attribute :from_latitude, Float
  attribute :from_longitude, Float
  attribute :to_latitude, Float
  attribute :to_longitude, Float

  def self.find(query, delivery_line)
    results = self.search(query)
    
    # remove any dups
    results.uniq { |result| result.delivery_line(delivery_line.primary_address_number, delivery_line.secondary_address) }
  end

  def delivery_line(address_number, secondary_address)
    delivery_line = "#{address_number} #{street_pre_direction_abbrev} #{street_name} #{street_suffix_abbrev} #{addr_secondary_abbrev} #{secondary_address}"
    delivery_line.squeeze(' ').strip
  end

  def last_line
    "#{city_name.strip}, #{state_abbrev} #{formatted_zip_code}"
  end

  def plus4_code
    return '' if self.zip_add_on_low_number_sector_number.blank?
    return '' if self.zip_add_on_high_number_sector_number.blank?
    return '' if self.zip_add_on_low_number_segment_number.blank?
    return '' if self.zip_add_on_high_number_segment_number.blank?
    return '' if self.zip_add_on_low_number_sector_number != self.zip_add_on_high_number_sector_number
    return '' if self.zip_add_on_low_number_segment_number != self.zip_add_on_high_number_segment_number
    "#{self.zip_add_on_low_number_sector_number}#{self.zip_add_on_low_number_segment_number}"
  end

  def formatted_zip_code
    return self.zip_code if self.plus4_code.blank?
    "#{self.zip_code}-#{self.plus4_code}"
  end
end


