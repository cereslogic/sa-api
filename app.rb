require File.join(File.dirname(__FILE__), 'config', 'environment.rb')

class App < Sinatra::Base
  def logger
    $logger
  end

  configure :production, :staging, :development do
    enable :logging
    enable :dump_errors
    enable :raise_errors
    enable :method_override
  end

  configure :production, :staging do
    log = File.new("log/sinatra.log", "a")
    STDOUT.reopen(log)
    STDERR.reopen(log)
  end

  set :protection, :origin_whitelist => ORIGIN_WHITELIST
  set :server, 'puma'

  # TODO: We'll need to do some stuff here to support CORS requests
  # per user ID.
  use Rack::Cors do
    allow do
      origins ORIGIN_WHITELIST
      resource '*',
          :methods => [:get, :post, :put, :delete, :options],
          :headers => :any
    end
  end

  use Rack::Session::Cookie, :secret => 'hljksfljsdfkweoriuwsl898723596iueroiuweroywer',
                             :httponly => false,
                             :domain => '.sureaddress.io'

  use Rack::Parser

  configure :production do
  end

  configure :staging do
  end

  configure :development do
  end

  get '/ping' do
    Time.now.to_json
  end

  def unauthorized!
    error 401
  end

  run! if app_file == $0

  private

  def token
    env['HTTP_X_SA_AUTH_ID'] || session['token']
  end

  def whitelisted?(req)
    url = "/" + req.path_info.split('/')[2..-1].join("/")
    PUBLIC_URLS.include?(url)
  end
end
